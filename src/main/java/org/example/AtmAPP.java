package org.example;

import org.example.employee.CLvlEmployee;
import org.example.employee.Employee;
import org.example.ui.Display;
import org.example.user.BankCustomer;

import static org.example.input.UserInput.authenticate;
import static org.example.user.UserActions.*;
import static org.example.utils.dataUtils.getCustomer;

public class AtmAPP {
    public static final String ATM_NAME = "Free Cash";
    public static void main(String[] args)
    {
        BankCustomer someone = getCustomer();

        //Display ui = new Display();
        Display.showWelcomemsg();
        boolean passThrough = authenticate(someone);
       if (!passThrough) {

            Display.lockUserFor24h();
           return;
       }

       Display.showMeniu();

       String option = someone.selectOptionMenu();
       switch (option){
           case "0" -> Display.repairAtm(initCLvlEmployee());
           case "1" ->{withdraw(someone);}
           case "2" -> Display.showCustomerBalanceSheet(someone.getBankAccount());
           case "3" -> {changePin(someone);}
           case "4" -> Display.activateCardMessage(someone);
           case "5" -> depositCash(someone);
           case "6" -> deactivateAccount(someone);
           case "7" -> Display.showIban(someone);
           case "8" -> Display.showCardNumber(someone.getBankAccount().getCard());


           }
        //Display.askUserForAmount();
        //someone.withdrawCash("15");
        //someone.selectOptionMenu();
        //someone.checkBalance();

    }

      private static Employee initEmployee(){
        return new Employee("1860601037891", "Dorel","Dorica", "Mr." );
      }
    private static CLvlEmployee initCLvlEmployee(){
        return new CLvlEmployee("1860601037891", "Tim","Cook", "Mr.", "CEO" );
    }
    //private static void authentucate(BankCustomer someone, Display ui) {
    //}

}

