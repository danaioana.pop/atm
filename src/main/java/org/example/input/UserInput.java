package org.example.input;

import org.example.ui.Display;
import org.example.user.BankCustomer;

import java.io.InputStream;
import java.util.Scanner;

public class UserInput {

    public static String readFromKeyBord (){
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard. next();

    }

    public static int readIntFromKeyBord() {
        InputStream in = System.in;
        Scanner keyboard = new Scanner(in);
        return keyboard.nextInt();
    }



        public static boolean authenticate(BankCustomer someone){
         for( int i = 0; i<3; i++) {
        String pinCode = Display.askForPin();
        boolean isValid = someone.validatePin(pinCode);
        if (isValid) {
            return true;
        }
             Display.displayInvalidMsg();

         }
         return false;
    }


}
