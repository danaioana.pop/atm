package org.example.utils;

import org.example.user.BankAccount;
import org.example.user.BankCustomer;
import org.example.user.card.Card;
import org.example.user.card.CredtCard;

public class dataUtils {
    public static BankCustomer getCustomer(){
        BankCustomer customer = new BankCustomer(1, "180000103931", "Dana", "Pop",9876,232);
        BankAccount account = new BankAccount("ROOORCNTNC01019185", " RON");
        account.updateBalance(150);
        Card card = new Card("5298759817365198","9876");
        account.setCard(card);
        customer.setAccount(account);

        return customer;
    }
}
