package org.example.user;

public class CreditCustomer extends Person {
    public CreditCustomer(String cnp, String firstName, String lastName) {
        super(cnp, firstName, lastName);
    }

    @Override
    public String getFullName() {
        return getFirstName() + " " +getLastName();
    }
}
