package org.example.user;

import org.example.output.ATMDispatcher;
import org.example.ui.Display;

public class UserActions {
    public static void withdraw(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        ATMDispatcher.withdraw(someone.getBankAccount(),amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }

    public static void changePin(BankCustomer someone) {
        String initialPin = Display.askForPin();
        boolean isValid = someone.validatePin(initialPin);
        if(!isValid){
            Display.displayInvalidMsg();
            return;
        }
        String changePin = Display.askForNewPin();
        String confirmedChangePin  = Display.confirmNewForPin();
        boolean pinMatches = someone.validateChangedPin(changePin, confirmedChangePin);
        if (!pinMatches) {
            Display.displayInvalidMsg();
            return;
        }
        someone.updatePin(changePin);
        System.out.println("Pin Correct. Update pin " + changePin + "and try again.");
    }
    public static void depositCash(BankCustomer someone) {
        int amount = Display.askUserForAmount();
        someone.depositCash(amount);
        Display.showCustomerBalanceSheet(someone.getBankAccount());
    }
    public static void deactivateAccount(BankCustomer someone) {
        someone.deactivateAccount();
        Display.informUserWithDeactivation();}
}

