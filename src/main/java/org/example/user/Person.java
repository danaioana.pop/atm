package org.example.user;

public abstract class Person {
    private final String cnp;
    private final String firstName;
    String lastName;
    public Person(String cnp, String firstName, String lastName) {
        this.cnp = cnp;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    protected String getFirstName() {
        return firstName;
    }

    protected String getLastName() {
        return lastName;
    }
    public abstract String getFullName();
}
