package org.example.user;

import org.example.user.card.Card;

public class BankAccount {
    private final String accountNumber;
    private final String currency;
    private double balance = 0;
    private Card card;



    public BankAccount(String accountNumber, String currency) {
        this.accountNumber = accountNumber;
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
    public double getBalance() {
        return balance;
    }

    public void updateBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return  accountNumber;
    }
}
