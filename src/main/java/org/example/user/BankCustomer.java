package org.example.user;

import org.example.input.UserInput;

public class BankCustomer extends Person  {
    private final int id;

    private final int pin;
    private final int balance;

    private BankAccount bankAccount;
    private boolean isActive;




    public BankCustomer(int bId, String cnp, String firstName, String lastName, int pin, int balance) {
        super(cnp, firstName, lastName);
        this.id = bId;
        this.pin = pin;
        this.balance = balance;
        this.isActive = true;
    }

    public void deactivateAccount(){
        this.isActive = false;
    }
    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }
    //public void validatePin(String input){
    //    System.out.println(" Person: " + firstName + " typed " + input);


        /*Scanner sc = new Scanner(System.in);
        int inputPin = sc.nextInt();

        if( inputPin != pin){
            System.out.println("Wrong pin. You have 2 more attempts");
        }else System.out.println("Correct pin");

        int secondTry = sc.nextInt();
        if( secondTry != pin){
            System.out.println("Wrong pin. You have 1 more attempt");
        }else System.out.println("Correct pin");

        int finalTry = sc.nextInt();
        if( finalTry != pin){
            System.out.println("You've entered your PIN wrong too many times. Your card was blocked");
            System.exit(1);
        }else System.out.println("Correct pin");*/

        /*for(int i=0; i<=2; i++){
            Scanner scx = new Scanner(System.in);
            int inputPin = scx.nextInt();

            if( inputPin != pin){
                System.out.println("Wrong PIN");
            }
            else {
                System.out.println("Correct PIN");
                break;
            }
            if(i ==2){
                System.out.println("You've entered a wrong PIN three times, your card is blocked!");
                System.exit(1);
            }

        }*/

    //}
    public boolean validatePin(String input){
        System.out.println(" Person: " + getFirstName() + " typed " + input);
        return bankAccount.getCard().verifyPin(input);


    }
    public void updatePin(String changePin) {
        bankAccount.getCard().updatePin(changePin);
    }
    public String selectOptionMenu(){
        String option = UserInput.readFromKeyBord();
        System.out.println("Person: "+ getFirstName() +" selected: " + option);
        return option;
    }
    public void withdrawCash(String input){
        System.out.println("Person " + getFirstName()+ " " + lastName + " withdraw " + input +" Lei" );

    }

    public void checkBalance(){
        System.out.println("Person " + getFirstName() + " has "+ balance + " Ron in the account" );
    }

    public boolean validateChangedPin(String changePin, String confirmedChangePin) {
        return changePin.equals(confirmedChangePin);
    }

    @Override
    public String getFullName() {
        return getFirstName() + " "+lastName;
    }

    public void depositCash(int amount) {
        double balance =bankAccount.getBalance();
        bankAccount.updateBalance(balance + amount);
    }
}
