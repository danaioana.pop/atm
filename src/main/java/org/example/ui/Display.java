package org.example.ui;

import org.example.employee.Employee;
import org.example.input.UserInput;
import org.example.user.BankAccount;
import org.example.user.BankCustomer;
import org.example.user.card.Card;

import static org.example.AtmAPP.ATM_NAME;

public class Display {

    public  static final String APP_MENU =
            """
                    Select an option:
                    1. Withdraw cash
                    2. Check balance
                    3. Change pin
                    4. Activate card
                    5. Deposit Cash
                    6. Deactivate Account
                    7. Show IBAN
                    8. Show card details
                   
                    """;
    public static void showWelcomemsg() {
        System.out.println("Welcome to" + ATM_NAME);
    }
    public  static String askForPin(){

        System.out.println("Please enter your Pin number: ");
        return readFromKeyBord();
    }

    public static String askForNewPin() {
        System.out.println("Please enter  new Pin number: ");
        return readFromKeyBord();
    }
    public static String confirmNewForPin() {
        System.out.println("Please confirm new Pin number: ");
       return readFromKeyBord();
    }
    private static String readFromKeyBord(){
        return UserInput.readFromKeyBord();
    }
   public static void displayInvalidMsg() {
        System.out.println("Pin incorrect,please try again.");
    }
    public  static void lockUserFor24h(){
        System.out.println("Incorrect pin,card locked for 24 hours");
    }
    public static void showMeniu(){
        System.out.println(APP_MENU);
    }

    public  static int askUserForAmount(){
        System.out.println("Type in amount you want : ");
        return UserInput.readIntFromKeyBord();
    }

    public  static void showCustomerBalanceSheet(BankAccount bankAccount) {
        System.out.println("Your Balance is: " + bankAccount.getBalance() +"" + bankAccount.getCurrency());
    }
    public  static void showNotEnoughMoneyMsg(){
        System.out.println("Your banck account does not have enough cash");
    }


    public static void activateCardMessage(BankCustomer someone) {
        System.out.printf("thank you %s for activating the card.", someone.getFullName());
    }

    public static void informUserWithDeactivation() {
        System.out.println("Thank you for being a valuable customer. Hope you come back");
    }

    public static void showIban(BankCustomer someone) {
        System.out.println( "IBAN; " +someone.getBankAccount().getAccountNumber());
    }

    public static void showCardNumber(Card card) {
        System.out.println("Card number is: " + card.getcardnumber());
    }

    public static void repairAtm(Employee employee) {
        System.out.println(" Employee: %s "+ employee.getFullName() + " is reapring yhe ATM");
    }
}
